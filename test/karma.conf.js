// Karma configuration
// Generated on Sun Oct 11 2015 03:42:32 GMT-0500 (Hora est. Pacífico, Sudamérica)

module.exports = function(config) {
'use strict';

  config.set({

    // base path that will be used to resolve all patterns (eg. files, exclude)
    basePath: '../public',


    // frameworks to use
    // available frameworks: https://npmjs.org/browse/keyword/karma-adapter
    frameworks: ['jasmine'],


    // list of files / patterns to load in the browser
    files: [
      // bower:js
      'components/jquery/dist/jquery.js',
      'components/FileSaver/FileSaver.js',
      'components/angular/angular.js',
      'components/angular-animate/angular-animate.js',
      'components/angular-bootstrap/ui-bootstrap-tpls.js',
      'components/bootstrap/dist/js/bootstrap.js',
      'components/angular-bootstrap-nav-tree/dist/abn_tree_directive.js',
      'components/angular-cookies/angular-cookies.js',
      'components/angular-resource/angular-resource.js',
      'components/angular-route/angular-route.js',
      'components/angular-sanitize/angular-sanitize.js',
      'components/angular-touch/angular-touch.js',
      'components/angularUtils-pagination/dirPagination.js',
      'components/ng-breadcrumbs/dist/ng-breadcrumbs.min.js',
      'components/angular-mocks/angular-mocks.js',
      // endbower
      "js/*.js",
      "js/**/*.js",
      "../test/spec/**/*.js"
    ],


    // list of files to exclude
    exclude: [
    ],


    // preprocess matching files before serving them to the browser
    // available preprocessors: https://npmjs.org/browse/keyword/karma-preprocessor
    preprocessors: {
    },


    // test results reporter to use
    // possible values: 'dots', 'progress'
    // available reporters: https://npmjs.org/browse/keyword/karma-reporter
    reporters: ['progress'],


    // web server port
    port: 8080,


    // enable / disable colors in the output (reporters and logs)
    colors: true,


    // level of logging
    // possible values: config.LOG_DISABLE || config.LOG_ERROR || config.LOG_WARN || config.LOG_INFO || config.LOG_DEBUG
    logLevel: config.LOG_INFO,


    // enable / disable watching file and executing tests whenever any file changes
    autoWatch: true,


    // start these browsers
    // available browser launchers: https://npmjs.org/browse/keyword/karma-launcher
    browsers: ["PhantomJS"],

    // Which plugins to enable
    plugins: [
      "karma-phantomjs-launcher",
      "karma-jasmine"
    ],

    // Continuous Integration mode
    // if true, Karma captures browsers, runs the tests and exits
    singleRun: false
  })
}
