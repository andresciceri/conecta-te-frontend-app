'use strict';

describe('conectateApp |', function () {

	var $scope, ctrl, $httpBackend;

	// load the controller's module
  	beforeEach(module('conectateApp'));
  	beforeEach(module('conectateServices'));

  	describe('HomeCtrl |',function (){

  		beforeEach(inject(function ($rootScope,$controller,_$httpBackend_){
  			$httpBackend = _$httpBackend_;

  			$scope 		= $rootScope.$new();
	  		ctrl = $controller('HomeCtrl',{
	  			$scope: $scope
	  		});
	  	}));

	  	it('Should have a items variable defined', function(){
  	 		expect($scope.items).toBeDefined();
  		});

  		it('The items per page should be 3', function(){
  	 		expect($scope.pageSize).toBe(3);
  		});

	  	it('Should have a totalItems variable defined', function(){
			expect($scope.totalItems).toBeDefined();
		});

		it('Should make a get request with the last items created', function (){
			$scope.getResultsPage("","");
			/*$httpBackend.expectGET('https://conectate-agiles.herokuapp.com/rest/items/last?expand=all').respond({
				items: []
			});
			$httpBackend.flush();*/
			expect($scope.items).toBeDefined();
		});

  	});
  	
});