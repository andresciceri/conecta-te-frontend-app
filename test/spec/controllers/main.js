'use strict';

describe('conectateApp |', function () {

	var $scope, ctrl, $httpBackend;

	// load the controller's module
  	beforeEach(module('conectateApp'));
  	beforeEach(module('conectateServices'));

    describe('MainCtrl |',function (){

  		beforeEach(inject(function ($rootScope,$controller,_$httpBackend_){
        
  			$httpBackend = _$httpBackend_;
  			$scope 		= $rootScope.$new();
	  		ctrl = $controller('MainCtrl',{
	  			$scope: $scope
	  		});
	  	}));

	  	it('Should have a items variable defined', function(){
  	 		expect($scope.items).toBeDefined();
  		});

  		it('The items per page should be 3', function(){
  	 		expect($scope.pageSize).toBe(3);
  		});

	  	it('Should have a totalItems variable defined', function(){
			expect($scope.totalItems).toBeDefined();
		});

  	});
  	
});