'use strict';

var request = require('request');

module.exports = function (grunt) {
  // show elapsed time at the end
  require('time-grunt')(grunt);
  // load all grunt tasks
  require('load-grunt-tasks')(grunt);

  var reloadPort = 35729, files;

  var jsText = "\nangular.module('conectateApp').value('config', '"+process.env.API_BACKEND+"');";

  grunt.loadNpmTasks('grunt-file-append');

  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    develop: {
      server: {
        file: 'bin/www'
      }
    },
    file_append: {
      default_options: {
          files: [
            {
              append: jsText,
              input: 'public/js/app.js'
            }
          ]
        }
    },
    watch: {
      options: {
        nospawn: true,
        livereload: reloadPort
      },
      server: {
        files: [
          'bin/www',
          'app.js',
          'routes/*.js'
        ],
        tasks: ['develop', 'delayed-livereload'],
        options: {
          livereload: reloadPort
        }
      },
      js: {
        files: ['public/js/*.js','public/js/**/*.js'],
        options: {
          livereload: reloadPort
        }
      },
      css: {
        files: [
          'public/css/*.css'
        ],
        options: {
          livereload: reloadPort
        }
      },
      views: {
        files: ['views/*.ejs'],
        options: {
          livereload: reloadPort
        }
      }
    },
    karma:{
        unit:{
          configFile: 'test/karma.conf.js',
          autoWatch: true,
          singleRun:true
        }
    }
  });

  grunt.config.requires('watch.server.files');
  files = grunt.config('watch.server.files');
  files = grunt.file.expand(files);

  grunt.registerTask('delayed-livereload', 'Live reload after the node server has restarted.', function () {
    var done = this.async();
    setTimeout(function () {
      request.get('http://localhost:' + reloadPort + '/changed?files=' + files.join(','),  function (err, res) {
          var reloaded = !err && res.statusCode === 200;
          if (reloaded) {
            grunt.log.ok('Delayed live reload successful.');
          } else {
            grunt.log.error('Unable to make a delayed live reload.');
          }
          done(reloaded);
        });
    }, 500);
  });

  grunt.registerTask('url-api','set the url api', function(){
      var data = grunt.file.readJSON('initConfig.json');
      data.apiUrl = process.env.API_BACKEND;
      grunt.file.write('public/js/apiData.json', JSON.stringify(data));
  });

  grunt.task.run('url-api');
  grunt.task.run('file_append');

  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-karma');
  
  
  
  grunt.registerTask('default', [
    'develop',
    'watch',
    'test',
    'url-api'
  ]);

  grunt.registerTask('test', [
    'karma'
  ]);

};
