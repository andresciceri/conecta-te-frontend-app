'use strict';

/**
 * @ngdoc overview
 * @name conectateApp
 * @description
 * # conectateApp
 *
 * Main module of the application.
 */
var app = angular
  .module('conectateApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch',
    'angularUtils.directives.dirPagination',
    'angularBootstrapNavTree',
    'ng-breadcrumbs',
    'ui.bootstrap',
    'conectateServices'
  ])
  .config(function ($routeProvider,paginationTemplateProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/home.html',
        controller: 'HomeCtrl',
        controllerAs: 'home',
        label: 'Inicio',
        resolve: {/*
          auth: function($q, authService){
              var userInfo = authService.getUserInfo();
              if(userInfo){
                return $q.when(userInfo);
              } else{
                return $q.reject({ authenticated: false });
              }
          }*/
        }
      })
      .when("/login", {
        templateUrl: "views/login.html",
        controller: "LoginController"
      })
      .when("/error", {
        templateUrl: "views/error.html"
      })
      .when('/results/collections/:idCol', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl',
        controllerAs: 'main',
        label: 'Resultados'
      })
      .when('/resources/:itemId', {
        templateUrl: 'views/itemView.html',
        controller: 'ItemCtrl',
        controllerAs: 'item',
        label: 'recurso'
      })
      .when('/results/items/:query', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl',
        controllerAs: 'main',
        label: 'Resultados'
      })
      .when('/users/', {
        templateUrl: 'views/usuarios.html',
        controller: 'UsersCtrl',
        controllerAs: 'users',
        label: 'Usuarios',
        resolve:{
            "check":function($q, authService){
                if(getCookie("accessAdmin") == ""){
                    window.location.href = "/#/error";
                }
            }
        }
      })
      .when('/users/create', {
        templateUrl: 'views/crearUsuario.html',
        controller: 'UserCreateCtrl',
        controllerAs: 'userCreate',
        label: 'CrearUsuario',
        resolve:{
            "check":function($q, authService){   //function to be resolved, accessFac and $location Injected
                if(getCookie("accessAdmin") == ""){
                    window.location.href = "/#/error";
                }
            }
        }
      })
      .when('/users/:userId',{
        templateUrl: 'views/userView.html',
        controller: 'UserCtrl',
        controllerAs: 'user',
        label: 'usuario'
      })
      .when('/users/edit/:userId',{
        templateUrl: 'views/userViewEdit.html',
        controller: 'UserEditCtrl',
        controllerAs: 'userEdit',
        label: 'usuario',
        resolve:{
            "check":function($q, authService){   //function to be resolved, accessFac and $location Injected
                if(getCookie("accessAdmin") == ""){
                    window.location.href = "/#/error";
                }
            }
        }
      })
      .when('/users/delete/:userId',{
        templateUrl: 'views/userViewDelete.html',
        controller: 'UserDeleteCtrl',
        controllerAs: 'userDelete',
        label: 'usuario',
        resolve:{
            "check":function($q, authService){   //function to be resolved, accessFac and $location Injected
                if(getCookie("accessAdmin") == ""){
                    window.location.href = "/#/error";
                }
            }
        }
      })
      .otherwise({
        redirectTo: '/'
      });
      paginationTemplateProvider.setPath('views/dirPagination.tpl.html');
  });
