'use strict';
/**
 * @ngdoc function
 * @name conectateApp.controller:UsersCtrl
 * @description
 * # UsersCtrl
 * Controller of the conectateApp
 */
angular.module('conectateApp')
 	.controller('UsersCtrl', ['$scope',
 							  '$http',
 							  'breadcrumbs',
 							  '$modal',
 							  'dataService',
 							  '$window',
 							  'Communities',
 							  'Items_Col',
 							  'Collections',
 							  'Search',
 							  'User',
 							  'Groups',
 							  'Users',
 							  '$route', function ($scope, $http,breadcrumbs,$modal,dataService,$window,Communities,Items_Col,Collections,Search,User,Groups,Users,$route) {

$scope.breadcrumbs = breadcrumbs;
 		 		
$scope.communities = Communities.get(function (data) {
	for (var i = 0; i < data.length; i++) {
	  	if (data[i].subcommunities.length > 0){
	    	for (var j = 0; j < data[i].subcommunities.length; j++) {
	      		data[i].subcommunities[j].collections = Collections.get({id: data[i].subcommunities[j].id});    
	    	};
	    
	  	}
	};

});

$scope.groups = Groups.list();
$scope.users = Users.get({},function(data){
	
	});

}]);