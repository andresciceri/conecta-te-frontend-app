'use strict';

/**
 * @ngdoc function
 * @name conectateApp.controller:ItemCtrl
 * @description
 * # ItemCtrl
 * Controller of the conectateApp
 */
 angular.module('conectateApp')
 	.controller('ItemCtrl', ['$scope',
 							  '$http',
 							  'breadcrumbs',
 							  '$modal',
 							  'dataService',
 							  '$window',
 							  'Item',
 							  'Communities',
 							  'Items_Col',
 							  'Collections',
 							  'Search',
 							  'config',
 							  '$route', function ($scope, $http,breadcrumbs,$modal,dataService,$window,Item,Communities,Items_Col,Collections,Search,$config,$route) {
 		
 		$scope.breadcrumbs = breadcrumbs;
 		 		
 		$scope.communities = Communities.get(function (data) {
	        for (var i = 0; i < data.length; i++) {
	          	if (data[i].subcommunities.length > 0){
	            	for (var j = 0; j < data[i].subcommunities.length; j++) {
	              		data[i].subcommunities[j].collections = Collections.get({id: data[i].subcommunities[j].id});    
	            	};
	            
	          	}
	      	};

	    });

 		$scope.id = $route.current.params.itemId;
 		$scope.item = Item.get({id:$scope.id},function(data){
 			$scope.item.meta = crearJsonI(data.metadata);
 			$scope.breadcrumbs.breadcrumbs[1] = {"label":data.parentCollection.name,"originalPath": "/results/collections/:query","param": data.parentCollection.id,"path": "/results/collections/"+data.parentCollection.id};
 			$scope.breadcrumbs.breadcrumbs[2] = {"label":$scope.item.meta['lom.general.title'],"originalPath": "/resources/:itemId","param": $scope.id,"path": "/resources/"+$scope.id};
 		});



    	function crearJsonI(elements){
	        var arAux = {};
	        for (var i = 0; i < elements.length; i++) {
	            arAux[elements[i].key] = elements[i].value;
	        }

	        return arAux;
    	}
		
	    $scope.open = function (size) {

		    $modal.open({
		      animation: $scope.animationsEnabled,
		      templateUrl: 'myModalContent.html',
		      controller: 'ModalInstanceCtrl',
		      size: size,
		      resolve: {
		        items: function () {
		          return $scope.item;
		        }
		      }
	    	});
	
		};

		$scope.openLOM = function(size){
			$modal.open({
				animation: $scope.animationsEnabled,
		      	templateUrl: 'myModalLOM.html',
		      	controller: 'ModalInstanceCtrl',
		      	size: size,
		      	resolve: {
		        items: function () {
		          	return $scope.item;
		        	}
		      	}
			});
		};

		$scope.downloadBit = function(){
			console.log($scope.item.bitstreams.length);
			for (var i = 0; i < $scope.item.bitstreams.length; i++) {
				getBit($scope.item.bitstreams[i].retrieveLink, $scope.item.bitstreams[i].mimeType,$scope.item.bitstreams[i].name);
			}
		};

		function getBit(path, bit, fileName){
			$http.get($config+'/rest'+path,{ responseType: 'arraybuffer' }).success(function(data){
		            
		            var file = new Blob([data], {type: bit});
		            saveAs(file, fileName);
		            
	        });
		}

		function jsonTojson(elements){
		    var treeArr = [];
		    if(elements.length > 0){
		        var i = 0;
		    	angular.forEach(elements, function(value){
		            treeArr[i] = {label : value.name, children : chlidrenElements(value), data : [{id: value.id}]};
		        	i++;
		    	}, treeArr);

		    }
		    return treeArr;
		}

	    function chlidrenElements(element){
	      var obj;
	      if (angular.isDefined(element.subcommunities) && angular.isArray(element.subcommunities) && element.subcommunities.length > 0) {
	                obj = jsonTojson(element.subcommunities);
	            }
	      return obj;
	    }

	    $scope.select_collection = function(id){
        	$window.location.href = "/#/results/collections/"+id;
    	};

    	$scope.search = function(){
      		$window.location.href = "/#/results/items/"+$scope.query;
    	};


 	}]);

angular.module('conectateApp').controller('ModalInstanceCtrl', function ($scope, $modalInstance, items) {

	$scope.item = items;

	$scope.ok = function () {
	    $modalInstance.close();
	};

	$scope.cancel = function () {
	    $modalInstance.dismiss('cancel');
	};
});