'use strict';

/**
 * @ngdoc function
 * @name conectateApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the conectateApp
 */
angular.module('conectateApp')
  .controller('MainCtrl', ['$scope',
                            '$http',
                            'breadcrumbs',
                            'Items',
                            'dataService',
                            '$window',
                            'Search',
                            'Communities',
                            'Collections',
                            'Items_Col',
                            'Item',
                            'Collection',
                            'Community',
                            '$route',
                            function ($scope, $http,breadcrumbs,Items,dataService,$window,Search,Communities,Collections,Items_Col,Item,Collection,Community,$route) {

    $scope.breadcrumbs = breadcrumbs;
    $scope.currentPage = 1;
  	$scope.pageSize = 3;
    $scope.communities=[];
    $scope.collections=[];
    $scope.hideCollections = true;
    $scope.items = [];
    $scope.totalItems = 0;
    
    //Uso de servicio REST para traer los items
    
    
    $scope.pagination = {
      current: 1
    };

    function getResultsPage(limit,offset,id){
      $scope.communities = Communities.get(function (data) {
        
        for (var i = 0; i < data.length; i++) {
            if (data[i].subcommunities.length > 0){
              for (var j = 0; j < data[i].subcommunities.length; j++) {
                data[i].subcommunities[j].collections = Collections.get({id: data[i].subcommunities[j].id});    
              };
              
            }
        };

      });
      
      if (limit === '' && offset === '') {
          
          $scope.items = Items_Col.get({id: id},function(data){

             for (var i = 0; i < data.length; i++) {
               
                $scope.items[i].meta = crearJsonI($scope.items[i].metadata);
                
              }
              $scope.totalItems = $scope.items.length;
              $scope.breadcrumbs.breadcrumbs[1] = {"label":$scope.items[0].parentCollection.name,"originalPath": "/results/:query","param": id,"path": "/results/"+id};
          });
      }else{

          $scope.items = Items_Col.get({id: id, limit: limit, offset: offset},function(data){

           for (var i = 0; i < data.length; i++) {
             
              $scope.items[i].meta = crearJsonI($scope.items[i].metadata);
              
            }
        });
      }
      
    }

    function setCommunities(){
      var col;
      for (col in $scope.collections_arr) {
        if($scope.collections_arr.hasOwnProperty(col)){  
          Collection.get({id:$scope.collections_arr[col]},function(data){
            Community.get({id:data.parentCommunity.id,expand:'parentCommunity'},function(data){
                Community.get({id:data.id,expand:'all'},function(data){
                    $scope.communities.push(data);
                });
            });  
          });
         } 
      };

    }

    function getResultsPageQuery(limit,offset,query){
      if (limit === '' && offset === '') {
          $scope.communities = [];
          $scope.collections_arr = [];
          $scope.items = Search.get({q:query},function (data){
            
            for (var i = 0; i < data.length; i++) {
                $scope.items[i] = Item.get({id:data[i].id},function(dataI){
                
                  $scope.collections_arr[i] = dataI.parentCollection.id;
                  dataI.meta = crearJsonI(dataI.metadata);
                  setTimeout(function(){
                    $scope.items[i].meta = dataI.meta;
                  },2000);

              });

            }

            $scope.totalItems = $scope.items.length;
            $scope.query = "";
          });

         setTimeout(function(){
             setCommunities();             
          },8000);
          
      }else{

          $scope.items = Search.get({q:query,limit: limit,offset: offset},function (data){
        
            for (var i = 0; i < data.length; i++) {
              $scope.items[i] = Item.get({id:data[i].id},function(dataI){

                dataI.meta = crearJsonI(dataI.metadata);
                $scope.items[i].meta = dataI.meta;
              });
            };
          });
      }
      
    }

    if(typeof($route.current) !== 'undefined'){
      
      if ( typeof($route.current.params.query) !== 'undefined' && $route.current.params.query !== "") {
          getResultsPageQuery('', '',$route.current.params.query);

          $scope.pageChanged = function(newPage){
            getResultsPageQuery($scope.pageSize,(newPage)*$scope.pageSize,$route.current.params.query);
          };
      }else {
          getResultsPage('', '', $route.current.params.idCol);

          $scope.pageChanged = function(newPage){
            getResultsPage($scope.pageSize,(newPage - 1)*$scope.pageSize,$route.current.params.idCol);
          };
      }
    }

    function crearJsonI(elements){
          var arAux = {};

          for (var i = 0; i < elements.length; i++) {
              arAux[elements[i].key] = elements[i].value;
          }

          return arAux;
      }

            
    /**
     ** Funcion para traer las colecciones pertencientes a una comunidad
     **/
    $scope.select_community = function(branch){
      
      $scope.community_name = branch.label;
      $scope.hideCollections = false;

      $scope.collections = Collections.get({id: branch.data[0].id}, function(data){
        var dataArr = angular.fromJson(data);
        $scope.dataTreeCollection = jsonTojson(dataArr);
      });
      
    };

    $scope.select_collection = function(id){
        $window.location.href = "/#/results/collections/"+id;
    };

    /**
    **Función que lee el json que llega del API y lo traduce a un objeto que entiende el plugin generador
    **del menú tipo arbol
    **/

    function jsonTojson(elements){
        var treeArr = [];
        if(elements.length > 0){
          var i = 0;
        angular.forEach(elements, function(value){
          
          treeArr[i] = {label : value.name, children : chlidrenElements(value), data : [{id: value.id}]};
          i++;
        }, treeArr);

      }
        return treeArr;
    }

    function chlidrenElements(element){
      var obj;
      if (angular.isDefined(element.subcommunities) && angular.isArray(element.subcommunities) && element.subcommunities.length > 0) {
                obj = jsonTojson(element.subcommunities);
            }
      return obj;
    }

    $scope.goItem = function(id){
      dataService.set(id);
      $window.location.href = "/#/resources/"+id;
    };

    $scope.search = function(){
      $window.location.href = "/#/results/items/"+$scope.query;
    };    
  }]);
