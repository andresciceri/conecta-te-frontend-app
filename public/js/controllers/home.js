'use strict';

/**
 * @ngdoc function
 * @name conectateApp.controller:HomeCtrl
 * @description
 * # HomeCtrl
 * Controller of the conectateApp
 */
angular.module('conectateApp')
  .controller('HomeCtrl', ['$scope',
                            '$http',
                            'breadcrumbs',
                            'Items',
                            'dataService',
                            '$window',
                            'Search',
                            'Communities',
                            'Collections',
                            'Items_Col',
                            'Item',
                            'ItemsLast',
                            '$route',
                            function ($scope, $http,breadcrumbs,Items,dataService,$window,Search,Communities,Collections, Items_Col,Item,ItemsLast,$route) {


    var loop = false;

    
    $scope.breadcrumbs = breadcrumbs;
    $scope.currentPage = 1;
  	$scope.pageSize = 3;
    $scope.totalItems = 0;
    $scope.items=[];
    $scope.communities=[];
    $scope.collections=[];
    $scope.hideCollections = true;
    $scope.dataTree = [];
    $scope.dataTreeCollection = [];
    var tree;

    //Uso de servicio REST para traer los items
    
    
    $scope.pagination = {
      current: 1
    };
       
    $scope.getResultsPage = function (limit,offset){
      if (limit === '' && offset === '') {
          $scope.items = ItemsLast.get(function success(data){
          
              for (var i = 0; i < data.length; i++) {
                
                data[i].meta =  crearJsonI(data[i].metadata);
                
              }
              $scope.items = data;
              $scope.totalItems = $scope.items.length;
          });  
      }else{
            $scope.items = ItemsLast.get({limit: limit,offset:offset},function success(data){
          
              for (var i = 0; i < data.length; i++) {
                
                data[i].meta =  crearJsonI(data[i].metadata);
                
              }
              $scope.items = data;
          });
      }
      
    }

    $scope.getResultsPage("", "");

    $scope.pageChanged = function(newPage){
      $scope.getResultsPage($scope.pageSize,(newPage - 1)*$scope.pageSize);
    };
    
    function crearJsonI(elements){
          var arAux = {};

          for (var i = 0; i < elements.length; i++) {
              arAux[elements[i].key] = elements[i].value;
          }

          return arAux;
      }

    if(typeof($route.current) !== 'undefined'){        
    $scope.communities = Communities.get(function (data) {
      for (var i = 0; i < data.length; i++) {
          if (data[i].subcommunities.length > 0){
            for (var j = 0; j < data[i].subcommunities.length; j++) {
              data[i].subcommunities[j].collections = Collections.get({id: data[i].subcommunities[j].id});    
            };
            
          }
      };

    });
  }

    /**
     ** Funcion para traer las colecciones pertencientes a una comunidad
     **/
    $scope.select_community = function(branch){
      
      $scope.community_name = branch.label;
      $scope.hideCollections = false;

      $scope.collections = Collections.get({id: branch.data[0].id}, function(data){
        var dataArr = angular.fromJson(data);
        $scope.dataTreeCollection = jsonTojson(dataArr);
        $scope.my_tree_col = tree = {};
      });
      
    };

    $scope.select_collection = function(id){
        $window.location.href = "/#/results/collections/"+id;
    };

    /**
    **Función que lee el json que llega del API y lo traduce a un objeto que entiende el plugin generador
    **del menú tipo arbol
    **/

    function jsonTojson(elements){
        var treeArr = [];
        if(elements.length > 0){
          var i = 0;
        angular.forEach(elements, function(value){
          
          treeArr[i] = {label : value.name, children : chlidrenElements(value), data : [{id: value.id}]};
          i++;
        }, treeArr);

      }
        return treeArr;
    }

    function chlidrenElements(element){
      var obj;
      if (angular.isDefined(element.subcommunities) && angular.isArray(element.subcommunities) && element.subcommunities.length > 0) {
                obj = jsonTojson(element.subcommunities);
            }
      return obj;
    }

    $scope.goItem = function(id){
      dataService.set(id);
      $window.location.href = "/#/resources/"+id;
    };

    $scope.search = function(){
      $window.location.href = "/#/results/items/"+$scope.query;
    };

  }]).directive('communities', function() {
  return {
    templateUrl: 'views/communities.html'
  };
}).directive('login', function() {
  return {
     templateUrl: 'views/login.html',
     controller: 'LoginCtrl'
  };
}).directive('search',function(){
  return{
    templateUrl: 'views/search.html'
  };
});

