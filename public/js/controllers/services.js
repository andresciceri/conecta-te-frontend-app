'use strict';

/**
 * @ngdoc function
 * @name conectateServices
 * @description
 * # 
 * Services REST for conectateApp
 */

 var conectateServices = angular.module('conectateServices',['ngResource','conectateApp']);

 conectateServices.factory('Items',['$resource','config',
 	function($resource, $config){
 		
 		return $resource($config + '/rest/items/',{},{
 			get: {method: 'GET', params:{limit:'@limit',offset: '@offset',expand: 'all'}, isArray: true}
 		});


 	}]);

conectateServices.factory('ItemsLast',['$resource', 'config',
 	function($resource,$config){
 		
 		return $resource($config + '/rest/items/last',{},{
 			get: {method: 'GET', params:{limit:'@limit',offset: '@offset',expand: 'all'}, isArray: true}
 		});


	}]); 


 conectateServices.factory('Item',['$resource', 'config',
 	function($resource, $config){
 		return $resource($config + '/rest/items/:id/',{},{
 			get: {method: 'GET', params:{id:'@id', expand: 'all'}}
 		});
 	}]);

 conectateServices.factory('Communities', ['$resource', 'config',
 	function($resource, $config){
 		return $resource($config + '/rest/communities/top-communities/',{},{
 			get: {method: 'GET', params:{id:'@id',expand:'all'}, isArray: true}
 		});
 	}]);

 conectateServices.factory('Collections', ['$resource', 'config',
 	function($resource, $config){
 		return $resource($config + '/rest/communities/:id/collections',{},{
 			get: {method: 'GET', params:{id:'@id'}, isArray: true}
 		});
 	}]);

 conectateServices.factory('Items_Col', ['$resource', 'config',
 	function($resource, $config){
 		return $resource($config + '/rest/collections/:id/items',{},{
 			get: {method: 'GET', params:{id:'@id',limit:'@limit',offset: '@offset',expand:'all'}, isArray: true}
 		});
 	}]);

 conectateServices.factory('Collection', ['$resource', 'config',
 	function($resource, $config){
 		return $resource($config + '/rest/collections/:id',{},{
 			get: {method: 'GET', params:{id:'@id',expand:'parentCommunity'}, isArray: false}
 		});
 	}]);

 conectateServices.factory('Community', ['$resource', 'config',
 	function($resource, $config){
 		return $resource($config + '/rest/communities/:id/',{},{
 			get: {method: 'GET', params:{id:'@id',expand:'@expand'}, isArray: false}
 		});
 	}]);

 conectateServices.factory('dataService', function() {
	 var savedData = {};
	 function set(data) {
	   savedData = data;
	 }
	 function get() {
	  return savedData;
	 }

	 return {
	  set: set,
	  get: get
	 };

	});

conectateServices.factory('Search',['$resource', 'config',
 	function($resource, $config){
 		return $resource($config + '/rest/items/search',{},{
 			get: {method: 'GET', params:{q:'@q'}, isArray: true}
 		});
 	}]);

conectateServices.factory('User', ['$resource','config',
 	function($resource, $config){
 		return $resource($config + '/rest/users/:id/',{},{
 			get: {method: 'GET', params:{id:'@id'}, isArray: false}
 		});
 	}]);

conectateServices.factory('Users', ['$resource','config',
 	function($resource, $config){
 		return $resource($config + '/rest/users/',{},{
 			get: {method: 'GET', params:{}, isArray: true}
 		});
 	}]);

conectateServices.factory('UserCreate',["$http","$q","$window","config",function($http,$q,$window,$config){
	function createUser(user,$scope){
		$http({
			method: 'POST',
			url: $config + '/rest/users/',
			data: user
		}).then(function successCallback(response){
			$scope.success = true;
			$window.location.href = "/#/users/"+response.data.id;
		}, function errorCallback(response){
			$scope.success = false;
			$window.location.href = "/#/users/";
		});

	}

	return {
        createUser: createUser
    };
}]);

conectateServices.factory('UserEdit',["$http","$q","$window","config",function($http,$q,$window,$config){
	function updateUser(user,$scope){
		$http({
			method: 'PUT',
			url: $config + '/rest/users/'+user.id+'/',
			data: user
		}).then(function successCallback(response){
			$scope.success = true;
			if($window.location.hash === "#/users/"+response.data.id){
				location.reload();
			}else{
				$window.location.href = "/#/users/"+response.data.id;
			}
		}, function errorCallback(response){
			$scope.success = false;
		});

	}

	function addGroup2User(user, $scope,$modal){
		$http({
			method: 'PUT',
			url: $config + '/rest/users/'+user.id+'/',
			data: user
		}).then(function successCallback(response){
			$scope.success = true;
			$modal.dismiss('cancel');
			location.reload();
		}, function errorCallback(response){
			$scope.success = false;	
		});
	}

	return {
        updateUser: updateUser,
        addGroup2User: addGroup2User
    };
}]);

conectateServices.factory('UserDelete',["$http","$q","$window","config",function($http,$q,$window,$config){
	function deleteUser(userId,$scope){
		$http({
			method: 'DELETE',
			url: $config + '/rest/users/'+userId+'/',
			data: userId
		}).then(function successCallback(response){
			$scope.success = true;
			$window.location.href = "/#/users/";
		}, function errorCallback(response){
			$scope.success = false;
		});

	}

	return {
        deleteUser: deleteUser
    };
}]);

conectateServices.factory('Groups', ['$resource', 'config',
 	function($resource,$config){
 		return $resource($config + '/rest/groups/:id',{},{
 			list: {method: 'GET', params:{id:'@id'}, isArray: true},
 			get: {method: 'GET', params:{id:'@id'}, isArray: false}
 		});
 	}]);

conectateServices.factory('Env', ["$http",function ($http) {

	function urlApi(){
		$http({
			method: 'GET',
			url: '/api/env'
		}).then(function successCallback(response){
			angular.module('conectateApp').constant("config", {
			    appName: "Conecta-Te",
			    appVersion: "1.0.0",
			    apiUrl: response.data.url
			});
			return response.data.url;
		}, function errorCallback(response){
			return response;
		});
 		
 	}

 	return {
    	urlApi: urlApi
    };
 }]);

conectateServices.factory("authService", ["$http","$q","$window","config",function ($http, $q, $window, $config) {
	var userInfo;

	function getUser(idUser){
 		 $http({
			method: 'GET',
			url: $config + '/rest/users/' + idUser
		}).then(function successCallback(response){
			return response;
		}, function errorCallback(response){
			return response;
		});
 	}

	function login(userName, password) {
        var deferred = $q.defer();

        $http({
			method: 'POST',
			url: $config + '/login/',
			data: JSON.stringify({ username: userName, password: password })
		}).then(function successCallback(response){
			console.log(response);
			userInfo = {
				accessToken: response.data.token,
				username: response.data.user.username
			};
			$window.sessionStorage["userInfo"] = JSON.stringify(userInfo);

			setCookie("accessId", response.data.user.id, 10);
			setCookie("accessUsername", response.data.user.username, 10);
			setCookie("accessGroups", response.data.user.groups.join(","), 10);

			if (response.data.user.groups.join(",").search("4") != -1){
				setCookie("accessAdmin", 1, 10);
			}

			$window.location.href = "/#";
			deferred.resolve(userInfo);
		}, function errorCallback(response){
			deferred.reject(response);
		});

        return deferred.promise;
    }

    function logout() {
        setCookie("accessId", 0, -1);
        setCookie("accessUsername", 0, -1);
        setCookie("accessGroups", 0, -1);
        setCookie("accessAdmin", 0, -1);
        $window.location.href = "/#";
    }

    function getUserInfo() {
        return userInfo;
    }

    function init() {
        if ($window.sessionStorage["userInfo"]) {
            userInfo = JSON.parse($window.sessionStorage["userInfo"]);
        }
    }

    init();

    return {
    	getUser: getUser,
        login: login,
        logout: logout,
        getUserInfo: getUserInfo
    };
}]);