'use strict';
/**
 * @ngdoc function
 * @name conectateApp.controller:UserCreateCtrl
 * @description
 * # UserCreateCtrl
 * Controller of the conectateApp
 */

  angular.module('conectateApp')
 	.controller('UserCreateCtrl', ['$scope',
 							  '$http',
 							  'breadcrumbs',
 							  '$modal',
 							  'dataService',
 							  '$window',
 							  'Communities',
 							  'Items_Col',
 							  'Collections',
 							  'Search',
 							  'User',
 							  'Groups',
 							  'UserCreate',
 							  '$route', function ($scope, $http,breadcrumbs,$modal,dataService,$window,Communities,Items_Col,Collections,Search,User,Groups,UserCreate,$route) {

$scope.success = false;
$scope.breadcrumbs = breadcrumbs;

$scope.communities = Communities.get(function (data) {
	for (var i = 0; i < data.length; i++) {
	  	if (data[i].subcommunities.length > 0){
	    	for (var j = 0; j < data[i].subcommunities.length; j++) {
	      		data[i].subcommunities[j].collections = Collections.get({id: data[i].subcommunities[j].id});    
	    	};
	    
	  	}
	};

});


$scope.id = $route.current.params.userId;


$scope.user = User.get({id:$scope.id},function(data){
			$scope.breadcrumbs.breadcrumbs[1] = {"label":"usuarios","originalPath": "/users/","param": "","path": "/users/"};
 			$scope.breadcrumbs.breadcrumbs[2] = {"label":$scope.user.username,"originalPath": "/users/:UserId","param": $scope.id,"path": "/users/"+$scope.id};
 			$scope.master = angular.copy($scope.user);
 		});


$scope.reset = function(form) {
    if (form) {
      form.$setPristine();
      form.$setUntouched();
    }
    $scope.user = angular.copy($scope.master);
  };

$scope.create = function(user){
	if($scope.userForm.$valid){
		UserCreate.createUser(user,$scope);
			
	}
};

$scope.select_collection = function(id){
	$window.location.href = "/#/results/collections/"+id;
};

$scope.search = function(){
		$window.location.href = "/#/results/items/"+$scope.query;
};
}]);