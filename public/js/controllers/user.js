'use strict';
/**
 * @ngdoc function
 * @name conectateApp.controller:UserCtrl
 * @description
 * # UserCtrl
 * Controller of the conectateApp
 */
 angular.module('conectateApp')
 	.controller('UserCtrl', ['$scope',
 							  '$http',
 							  'breadcrumbs',
 							  '$modal',
 							  'dataService',
 							  '$window',
 							  'Communities',
 							  'Items_Col',
 							  'Collections',
 							  'Search',
 							  'User',
 							  'Groups',
 							  'UserEdit',
 							  '$timeout',
 							  '$route', function ($scope, $http,breadcrumbs,$modal,dataService,$window,Communities,Items_Col,Collections,Search,User,Groups,UserEdit,$timeout,$route) {

$scope.breadcrumbs = breadcrumbs;
 		 		
$scope.communities = Communities.get(function (data) {
	for (var i = 0; i < data.length; i++) {
	  	if (data[i].subcommunities.length > 0){
	    	for (var j = 0; j < data[i].subcommunities.length; j++) {
	      		data[i].subcommunities[j].collections = Collections.get({id: data[i].subcommunities[j].id});    
	    	};
	    
	  	}
	};

});

$scope.id = $route.current.params.userId;
$scope.groups = {};
$scope.user = User.get({id:$scope.id},function(data){
			$scope.breadcrumbs.breadcrumbs[1] = {"label":"usuarios","originalPath": "/users/","param": "","path": "/users/"};
 			$scope.breadcrumbs.breadcrumbs[2] = {"label":$scope.user.username,"originalPath": "/users/:UserId","param": $scope.id,"path": "/users/"+$scope.id};
 			if(data.groups.length > 0){
 				for (var i = 0; i < data.groups.length; i++) {

 					Groups.get({id:data.groups[i]},function(dataGrp){
	 					
	 					$scope.groups[dataGrp.id] = dataGrp;
	 				});
 				};

 			}
 		});

if(getCookie("accessAdmin") == "" && getCookie("accessId") != $route.current.params.userId)
{
	$window.location.href = "#/error";
}


$scope.addGroupUser = function(size){
	$modal.open({
			animation: $scope.animationsEnabled,
	      	templateUrl: 'myModalGroupSelect.html',
	      	controller: 'ModalGroupAddCtrl',
	      	size: size,
	      	resolve: {
	        groupsAdd: function () {
	          	return Groups.list({},function(data){

	          	});
	        	},
	        user: function(){
	        	return $scope.user;
	        	}
	      	}
		});
}


$scope.deleteGroupUser = function(idGroup, user){
	
	var i = user.groups.indexOf(idGroup);
	user.groups.splice(i,1);
	UserEdit.updateUser(user,$scope);
}

$scope.select_collection = function(id){
	$window.location.href = "/#/results/collections/"+id;
};

$scope.search = function(){
		$window.location.href = "/#/results/items/"+$scope.query;
};

}]);

angular.module('conectateApp').controller('ModalGroupAddCtrl',['$scope',
																'$modalInstance',
																'groupsAdd',
																'user',
																'UserEdit',
																 function ($scope, $modalInstance, groupsAdd,user,UserEdit) {

    $scope.groupsAdd = {};
	$scope.groupsAdd = groupsAdd;

	$scope.add = function (id) {
		if(user.groups.indexOf(parseInt(id)) == -1){
			user.groups.push(parseInt(id));
			UserEdit.addGroup2User(user,$scope,$modalInstance);
		}else{
			alert("El rol ya ha sido asginado");
		}
	};

	$scope.cancel = function () {
	    $modalInstance.dismiss('cancel');
	};
}]);