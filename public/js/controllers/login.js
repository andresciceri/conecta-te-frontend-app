'use strict';

/**
 * @ngdoc function
 * @name conectateApp.controller:LoginCtrl
 * @description
 * # LoginCtrl
 * Controller of the conectateApp
 */

 angular.module('conectateApp')
 	.controller('LoginCtrl', ['$scope',
 							  '$http',
 							  'breadcrumbs',
 							  '$modal',
 							  'dataService',
 							  '$window',
 							  'authService',
 							  '$route', function ($scope, $http,breadcrumbs,$modal,dataService,$window,authService,$route) {

$scope.userInfo = null;
$scope.isAdmin = null;

if (getCookie('accessId') != "")
{
    $scope.userInfo = {"accessToken": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9","username": getCookie('accessUsername'), "id": getCookie('accessId'), "groups": getCookie('accessGroups').split(",")};

    if (getCookie("accessAdmin") != "")
    {
        $scope.isAdmin = true;
    }
}

$scope.login = function(){
	authService.login($scope.username, $scope.password)
        .then(function (result) {
            $scope.userInfo = result;

            if (getCookie("accessAdmin") != "")
            {
                $scope.isAdmin = true;
            }
            else
            {
                $scope.isAdmin = null;
            }

            console.log(result);
            $window.location.replace("/#");
            return false;
        }, function (error) {
            $window.alert("Usuario o password inválido");
            console.log(error);
        });
};

$scope.logout = function () {
	$scope.userInfo = null;
    authService.logout();
    $window.location.replace("/#");
    };
}]);

function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays*24*60*60*1000));
    var expires = "expires="+d.toUTCString();
    document.cookie = cname + "=" + cvalue + "; " + expires;
}

function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i=0; i<ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1);
        if (c.indexOf(name) == 0) return c.substring(name.length,c.length);
    }
    return "";
}