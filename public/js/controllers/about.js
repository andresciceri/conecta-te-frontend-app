'use strict';

/**
 * @ngdoc function
 * @name conectateApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the conectateApp
 */
angular.module('conectateApp')
  .controller('AboutCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
