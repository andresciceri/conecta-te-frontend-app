var express = require('express');
var router = express.Router();

/* GET home page. */

router.get('/', function(req, res) {
  res.render('index', { title: 'Conecta-Te',framework:'AngularJS' });
});

router.get('/api/env', function(req, res){
	console.log(process.env);
	console.log('Env variable ' + process.env.API_BACKEND);
	var env = process.env.API_BACKEND;
    res.json({url: env});
});

module.exports = router;
