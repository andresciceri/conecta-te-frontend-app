# README #

This is an Express application to create a frontend stack for Conecta-Te system. This frontend uses a [Conecta-Te Api](https://bitbucket.org/nbonet/conecta-te-dspace), this Api gets the information from DSpace system.

[ ![Codeship Status for andresciceri/Conecta-Te Frontend App](https://codeship.com/projects/a41b5d80-53b5-0133-e97b-2aa02ad0cd50/status?branch=master)](https://codeship.com/projects/108392)

[![Codacy Badge](https://api.codacy.com/project/badge/6de5559029424697bc01a509b0665a22)](https://www.codacy.com/app/andresciceri/conecta-te-frontend-app)
 

## What is this repository for? ##

* This repository has 2 folders: The first called template-conectate has the Html templates with the design updated. The second folder is conectate-express who contains the express framework with the frontend stack (NodeJs & AngularJs).
* Version 1.0.0
* You can visit the app [Conecta-Te](https://conecta-te.herokuapp.com)

## How do I get set up? ##

* The application is developed with [Express JS Framework](http://expressjs.com/es/) and [AngularJS](https://angularjs.org/)

### Prerequisites ###
Before to continue with the installation you need install Node, NPM and Bower in your computer:

```
#!commands

C:\> sudo apt-get update
C:\> sudo apt-get install node
C:\> sudo apt-get install npm
C:\> npm install -g bower
```
### Configuration ###
You can change the configuration in the .\bin\www file:

```
#!javascript

#!/usr/bin/env node
var app = require('../app');

app.set('port', process.env.PORT || 3000);

var server = app.listen(app.get('port'), function() {
  console.log('Express server listening on port ' + server.address().port);
});

```
By default the server listen on port 3000.

### Dependencies ###
The express application has the next dependencies:

```
#!javascript

"dependencies": {
    "express": "^4.13.3",
    "serve-favicon": "^2.3.0",
    "morgan": "^1.6.1",
    "cookie-parser": "^1.3.3",
    "body-parser": "^1.13.3",
    "ejs": "^2.3.1",
    "bower": "1.5.2"
  },
  "devDependencies": {
    "grunt": "^0.4.5",
    "grunt-develop": "^0.4.0",
    "grunt-contrib-watch": "^0.6.1",
    "request": "^2.60.0",
    "time-grunt": "^1.2.1",
    "load-grunt-tasks": "^3.2.0",
    "karma-jasmine": "*",
    "karma-phantomjs-launcher": "*",
    "bower": "1.5.2"
  }
```
### Installation ###
You need to run the next commands to install the application:

```
#!commands
C:\> cd conectacte-express
C:\conectacte-express\> npm install
C:\conectacte-express\> bower install 

```
And with the next command you can run the application:

```
#!commands

C:\conectacte-express\> npm start
```
